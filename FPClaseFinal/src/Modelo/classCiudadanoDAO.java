
package Modelo;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class classCiudadanoDAO {
    
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    public boolean RegistrarCiudadano(classCiudadano citizen){
        String sql = "INSERT INTO tb_ciudadano(cedula, nombre, telefono, correo) VALUES (?, ?, ?, ?)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, citizen.getCedula());
            ps.setString(2, citizen.getNombre());
            ps.setString(3, citizen.getTelefono());
            ps.setString(4, citizen.getCorreo());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        } finally{
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public List ListarCiudadano(){
        List<classCiudadano> ListaCitizen = new ArrayList();
        String sql = "SELECT *FROM tb_ciudadano";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                classCiudadano citizen = new classCiudadano();
                citizen.setIdciudadano(rs.getInt("idciudadano"));
                citizen.setCedula(rs.getString("cedula"));
                citizen.setNombre(rs.getString("nombre"));
                citizen.setTelefono(rs.getString("telefono"));
                citizen.setCorreo(rs.getString("correo"));
                ListaCitizen.add(citizen);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return ListaCitizen;
    }
    
    public boolean EliminarCiudadano(int idciudadano){
        String sql = "DELETE FROM tb_ciudadano WHERE idciudadano = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idciudadano);
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally{
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public boolean ModificarCiudadano(classCiudadano citizen){
        
        String sql = "UPDATE tb_ciudadano SET cedula=?, nombre=?, telefono=?, correo=? WHERE idciudadano=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, citizen.getCedula());
            ps.setString(2, citizen.getNombre());
            ps.setString(3, citizen.getTelefono());
            ps.setString(4, citizen.getCorreo());
            ps.setInt(5, citizen.getIdciudadano());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public classCiudadano Login(String cedula, String nombre){
        classCiudadano citizen = new classCiudadano();
        String sql = "SELECT *FROM tb_ciudadano WHERE cedula = ? AND nombre = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, cedula);
            ps.setString(2, nombre);
            rs = ps.executeQuery();
            if (rs.next()){
                citizen.setIdciudadano(rs.getInt("idciudadano"));
                citizen.setCedula(rs.getString("cedula"));
                citizen.setNombre(rs.getString("nombre"));
                citizen.setTelefono(rs.getString("telefono"));
                citizen.setCorreo(rs.getString("correo"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return citizen;
    }
    

}
