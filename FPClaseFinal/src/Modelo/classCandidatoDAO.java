
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class classCandidatoDAO {
    
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
    public boolean RegistrarCandidato(classCandidato leader){
        String sql = "INSERT INTO tb_candidato(cedula, nombre, telefono, partido, ciudad, descripcion, mensaje, propuesta) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, leader.getCedula());
            ps.setString(2, leader.getNombre());
            ps.setString(3, leader.getTelefono());
            ps.setString(4, leader.getPartido());
            ps.setString(5, leader.getCiudad());
            ps.setString(6, leader.getDescripcion());
            ps.setString(7, leader.getMensaje());
            ps.setString(8, leader.getPropuesta());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        } finally{
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public List ListarCandidato(){
        List<classCandidato> ListaCitizen = new ArrayList();
        String sql = "SELECT *FROM tb_candidato";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                classCandidato citizen = new classCandidato();
                citizen.setIdcandidato(rs.getInt("idcandidato"));
                citizen.setCedula(rs.getString("cedula"));
                citizen.setNombre(rs.getString("nombre"));
                citizen.setTelefono(rs.getString("telefono"));
                citizen.setPartido(rs.getString("partido"));
                citizen.setCiudad(rs.getString("ciudad"));
                citizen.setDescripcion(rs.getString("descripcion"));
                citizen.setMensaje(rs.getString("mensaje"));
                citizen.setPropuesta(rs.getString("propuesta"));
                ListaCitizen.add(citizen);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return ListaCitizen;
    }
    
    public boolean EliminarCandidato(int idcandidato){
        String sql = "DELETE FROM tb_candidato WHERE idcandidato = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idcandidato);
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally{
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public boolean ModificarCandidato(classCandidato leader){
        
        String sql = "UPDATE tb_candidato SET cedula=?, nombre=?, telefono=?, partido=?, ciudad=?, descripcion=?, mensaje=?, propuesta=? WHERE idcandidato=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, leader.getCedula());
            ps.setString(2, leader.getNombre());
            ps.setString(3, leader.getTelefono());
            ps.setString(4, leader.getPartido());
            ps.setString(5, leader.getCiudad());
            ps.setString(6, leader.getDescripcion());
            ps.setString(7, leader.getMensaje());
            ps.setString(8, leader.getPropuesta());
            ps.setInt(9, leader.getIdcandidato());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
    }
    
    public void ConsultarPartido (JComboBox partido){
        
        String sql = "SELECT nombrepartido FROM tb_partido";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                partido.addItem(rs.getString("nombrepartido"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }
    
    public void ConsultarCiudad (JComboBox ciudad){
        
        String sql = "SELECT nombrecuidad FROM tb_ciudad";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                ciudad.addItem(rs.getString("nombrecuidad"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }
    
}
