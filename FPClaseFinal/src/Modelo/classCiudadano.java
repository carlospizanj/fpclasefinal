
package Modelo;

public class classCiudadano {
    
    private int idciudadano;
    private String cedula;
    private String nombre;
    private String telefono;
    private String correo;

    public classCiudadano() {
    }

    public classCiudadano(int idciudadano, String cedula, String nombre, String telefono, String correo) {
        this.idciudadano = idciudadano;
        this.cedula = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
        this.correo = correo;
    }
    
    public int getIdciudadano() {
        return idciudadano;
    }

    public void setIdciudadano(int idciudadano) {
        this.idciudadano = idciudadano;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
        
}
