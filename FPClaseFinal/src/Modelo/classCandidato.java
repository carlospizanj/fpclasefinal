
package Modelo;

public class classCandidato {
    
    private int idcandidato;
    private String cedula;
    private String nombre;
    private String telefono;
    private String partido;
    private String ciudad;
    private String descripcion;
    private String mensaje;
    private String propuesta;

    public classCandidato() {
    }

    public classCandidato(int idcandidato, String cedula, String nombre, String telefono, String partido, String ciudad, String descripcion, String mensaje, String propuesta) {
        this.idcandidato = idcandidato;
        this.cedula = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
        this.partido = partido;
        this.ciudad = ciudad;
        this.descripcion = descripcion;
        this.mensaje = mensaje;
        this.propuesta = propuesta;
    }

    public int getIdcandidato() {
        return idcandidato;
    }

    public void setIdcandidato(int idcandidato) {
        this.idcandidato = idcandidato;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPartido() {
        return partido;
    }

    public void setPartido(String partido) {
        this.partido = partido;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getPropuesta() {
        return propuesta;
    }

    public void setPropuesta(String propuesta) {
        this.propuesta = propuesta;
    }
        
}
