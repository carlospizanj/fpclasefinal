
package Modelo;

public class classVotacion {
    
    private int idvotacion;
    private String cedulaciudadano;
    private String nombrecandidato;

    public classVotacion() {
    }

    public classVotacion(int idvotacion, String cedulaciudadano, String nombrecandidato) {
        this.idvotacion = idvotacion;
        this.cedulaciudadano = cedulaciudadano;
        this.nombrecandidato = nombrecandidato;
    }

    public int getIdvotacion() {
        return idvotacion;
    }

    public void setIdvotacion(int idvotacion) {
        this.idvotacion = idvotacion;
    }

    public String getCedulaciudadano() {
        return cedulaciudadano;
    }

    public void setCedulaciudadano(String cedulaciudadano) {
        this.cedulaciudadano = cedulaciudadano;
    }

    public String getNombrecandidato() {
        return nombrecandidato;
    }

    public void setNombrecandidato(String nombrecandidato) {
        this.nombrecandidato = nombrecandidato;
    }

    
    
}
